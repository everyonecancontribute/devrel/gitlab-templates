<!-- Please use the format below to add an Issue title. Dates should be using ISO dates, see https://about.gitlab.com/handbook/communication/#writing-style-guidelines -->

<!-- CFP: <Event Name>, <Location>, <Event Dates> - Due: <CFP Due Date> -->

## Event Details

* `Name of Event`: 
* `Event Website`:
* `Event Dates`: 
* `Location`: 
* `CFP Closes`: 
* `CFP Notification Date`:  
* `CFP Submission Link`: 
* `GitLab Event Issue/Epic`: <!-- Link to GitLab Developer Evangelism issue, if existing. --->


## Event Requirements

Follow the GitLab guidelines for speaker diversity. 

* [ ] Review and acknowledge the [event requirements](https://about.gitlab.com/handbook/marketing/corporate-communications/speaking-resources/#event-requirements) for speakers
  * [ ] Assign yourself and set the due date to CfP notifications.
  * [ ] Review the speakers lineup for event requirements, when announced.

## Co-ordination

### Submissions

You can use this [Google doc CFP template](https://docs.google.com/document/d/1vF9i4ZaR1_u52r_vUcVls0uhQIi7gzG9WkQ6l369FU4/edit?usp=sharing). Make a copy, update it, and add it into the issue comments/table for submission review. 


| Speaker(s)  | Topic   | Link to submission draft (where available) | Submission Status |
|-------------|---------|--------------------------------------------|-------------------|
|             |         |                                            |                   |
|             |         |                                            |                   |
|             |         |                                            |                   |


## Checklist

* [ ] Discuss submission ideas with @dnsmichi 
* [ ] Prepare abstract Google docs including title, abstract, benefits for the ecosystem, learning goals, etc. 
* [ ] Prepare Bio/CV doc 
* [ ] Add `CFP::Accepted` and `CFP-Accepted::1` labels when accepted (quick action: `/label ~"CFP::Accepted" ~"CFP-Accepted::1"`) 
* [ ] Promote speaker's cards on social media when allowed to share

## Resources 

- [CFP template Google doc](https://docs.google.com/document/d/1vF9i4ZaR1_u52r_vUcVls0uhQIi7gzG9WkQ6l369FU4/edit#).
- [Biography template Google doc](https://docs.google.com/document/d/1e_Sk0OGpKjWbs8C3xrIk33cShvTgoqRO3r1aQApiB5M/edit)


FYI: @dnsmichi 

/assign @priyaaakansha 

<!-- Please leave the label below on this issue https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/workflow/#cfp-workflow -->

/label ~"CFP::Open" 

<!-- Set a due date either until the CFP end date, or the event date using: /due April 17 -->
